import Vue from 'vue'
import {
  Button,
  Tab,
  Tabs,
  Lazyload,
  Image,
  Swipe,
  SwipeItem,
  Sticky,
  Toast,
  Grid,
  GridItem,
  Tabbar,
  TabbarItem,
  Icon,
  Divider,
  ImagePreview,
  SubmitBar,
  Card,
  Stepper
} from 'vant'

Vue.use(Button)
Vue.use(Tab)
Vue.use(Tabs)
Vue.use(Image)
Vue.use(Lazyload, {
  lazyComponent: true
})
Vue.prototype.$message = Toast
Vue.use(Swipe)
Vue.use(SwipeItem)
Vue.use(Sticky)
// Vue.use(Toast)
Vue.use(Grid)
Vue.use(GridItem)
Vue.use(Tabbar)
Vue.use(TabbarItem)
Vue.use(Icon)
Vue.use(Divider)
Vue.use(ImagePreview)
Vue.use(SubmitBar)
Vue.use(Card)
Vue.use(Stepper)
