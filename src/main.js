import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './Vant'
import axios from 'axios'
import './assets/css/global.css'
import './assets/font/iconfont.css'
import Footer from './components/Footer.vue'
import Head from './components/Head.vue'
axios.defaults.baseURL = 'http://www.liulongbin.top:3005/'
Vue.prototype.$http = axios

// 底部组件
Vue.component('Footer', Footer)
// 头部组件
Vue.component('Head', Head)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
