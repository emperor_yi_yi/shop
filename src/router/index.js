import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home.vue'
import ImageList from '../components/ImageList.vue'
import Member from '../components/Member.vue'
import ImageDetails from '../components/ImageDetails.vue'
import Shoppingcart from '../components/Shoppingcart.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/home' },
  { path: '/home', component: Home },
  { path: '/member', component: Member },
  // 图片列表
  { path: '/image_list', component: ImageList },
  // 图片详情
  { path: '/image_details/:id', component: ImageDetails },
  { path: '/shoppingcart', component: Shoppingcart }
]

const router = new VueRouter({
  routes
})

export default router
